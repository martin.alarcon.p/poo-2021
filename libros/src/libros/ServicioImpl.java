package libros;

import java.util.List;
import java.util.ArrayList;

public class ServicioImpl implements Servicio{
    List<Libro> libros=new ArrayList<>();
	public void buscarLibroPorTitulo(String titulo) {
		for (Libro libro:libros) {
			if (libro.getTitulo().equals(titulo)){
			     System.out.println("Libro encontrado");
			     datosLibro(libro);
			}
		}
	}

	public void buscarLibroPorAutores(String autores) {
		for (Libro libro:libros) {
			if (libro.getAutores().equals(autores)){
			     System.out.println("Libro encontrado");
			     datosLibro(libro);
			}
		}
	}

	public void buscarLibroPorCodigo(String codigo) {
		for (Libro libro:libros) {
			if (libro.getCodigo().equals(codigo)){
			     System.out.println("Libro encontrado");
			     datosLibro(libro);
			}
		}
		
	}
    public void datosLibro(Libro libro) {
    	System.out.println(libro.getTitulo());
	     System.out.println(libro.getAutores());
	     System.out.println(libro.getCodigo());
	     System.out.println(libro.getAņoPublicacion());
	     System.out.println(libro.getCapitulo());
	     System.out.println(libro.getDescripcion());
    }

	public void setLibros(List<Libro> libros) {
		this.libros = libros;
	}
    
}
