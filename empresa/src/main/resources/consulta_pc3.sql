USE empresa;
CREATE TABLE departamento(
   codigo_departamento VARCHAR(10) PRIMARY KEY,
   nombre VARCHAR(100),
   ubicacion VARCHAR(50)
);

CREATE TABLE empleado(
    codigo_empleado VARCHAR(20) PRIMARY KEY,
    codigo_departamento VARCHAR(10),
	 nombres VARCHAR(50),
	 apellidos VARCHAR(100));

CREATE TABLE asignacion(
    id_asignacion NUMERIC(5) PRIMARY KEY,
	 codigo_empleado VARCHAR(20),
	 id_actividad  NUMERIC(4),
	 presupuesto NUMERIC(9,2));
	 
CREATE TABLE actividad(
    id_actividad	NUMERIC(4) PRIMARY KEY,
    nombre VARCHAR(50),
    prioridad NUMERIC(1));
    
INSERT INTO departamento(codigo_departamento,nombre,ubicacion)
VALUES("I2","Consultas","Tercer Piso");

INSERT INTO departamento(codigo_departamento,nombre,ubicacion)
VALUES("A4","Impresoras","Base");

INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos)
VALUES("A111","I2","Benito","Benitez");
    
INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos)
VALUES("B111","A4","Jorge","Gonzalez");

INSERT INTO empleado(codigo_empleado,codigo_departamento,nombres,apellidos)
VALUES("C111","I2","Gustavo","Cerati");

INSERT INTO asignacion(id_asignacion,codigo_empleado,id_actividad,presupuesto)
VALUES(123,"A111",12,123.4);

--Obtener los nombres, apellidos y el nombre del departamento de todos los empleados.

SELECT e.nombres,e.apellidos,d.nombre nombre_departamento
FROM empleado e 
JOIN departamento d ON (e.codigo_departamento=d.codigo_departamento);

--Registrar una nueva actividad.

INSERT INTO actividad(id_actividad,nombre,prioridad)
VALUES(12,"Imprime",1);

--Obtener las asignaciones de un determinado empleado.

SELECT *
FROM asignacion
WHERE codigo_empleado="A111";

