package com.microservicio.empresa.dto;

public class Asignacion {
   private Integer id_asignacion;
   private String nombre;
   private Integer id_actividad;
   private Double presupuesto;
public Integer getId_asignacion() {
	return id_asignacion;
}
public void setId_asignacion(Integer id_asignacion) {
	this.id_asignacion = id_asignacion;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public Integer getId_actividad() {
	return id_actividad;
}
public void setId_actividad(Integer id_actividad) {
	this.id_actividad = id_actividad;
}
public Double getPresupuesto() {
	return presupuesto;
}
public void setPresupuesto(Double presupuesto) {
	this.presupuesto = presupuesto;
}
   
}
