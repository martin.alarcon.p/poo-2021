package com.microservicio.empresa.dto;

public class Empleado {
     private String codigo_empleado;
     private String codigo_departamento;
     private Departamento departamento;
     private String nombres;
     private String apellidos;
	public String getCodigo_empleado() {
		return codigo_empleado;
	}
	public void setCodigo_empleado(String codigo_empleado) {
		this.codigo_empleado = codigo_empleado;
	}
	public String getCodigo_departamento() {
		return codigo_departamento;
	}
	public void setCodigo_departamento(String codigo_departamento) {
		this.codigo_departamento = codigo_departamento;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public Departamento getDepartamento() {
		return departamento;
	}
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
     
}
