package com.microservicio.empresa.dto.rest;

import java.util.List;

import com.microservicio.empresa.dto.Asignacion;

public class RespuestaAsignacion {
    private List<Asignacion> listaAsignacion;

	public List<Asignacion> getListaAsignacion() {
		return listaAsignacion;
	}

	public void setListaAsignacion(List<Asignacion> listaAsignacion) {
		this.listaAsignacion = listaAsignacion;
	}
    
}
