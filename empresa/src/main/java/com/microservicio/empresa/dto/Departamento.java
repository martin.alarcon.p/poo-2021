package com.microservicio.empresa.dto;

public class Departamento {
    private String codigo_departamento;
    private String nombre;
    private String ubicacion;
	public String getCodigo_departamento() {
		return codigo_departamento;
	}
	public void setCodigo_departamento(String codigo_departamento) {
		this.codigo_departamento = codigo_departamento;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
    
}
