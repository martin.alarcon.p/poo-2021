package com.microservicio.empresa.servicio;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.microservicio.empresa.dao.Dao;
import com.microservicio.empresa.dto.*;
@Service
@Transactional
public class ServicioImpl implements Servicio{
	@Autowired
    private Dao dao;
	public List<Empleado> obtenerEmpleados() throws SQLException {
		List<Empleado> lista=new ArrayList<>();
		lista=dao.obtenerEmpleados();
		return lista;
	}
	public List<Asignacion> obtenerAsignacion(Empleado empleado) throws SQLException{
		List<Asignacion> lista=new ArrayList<>();
		lista=dao.obtenerAsignaciones(empleado);
		return lista;
	}
    
}
