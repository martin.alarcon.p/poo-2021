package com.microservicio.empresa.servicio;

import java.sql.SQLException;
import java.util.List;

import com.microservicio.empresa.dto.Asignacion;
import com.microservicio.empresa.dto.Empleado;

public interface Servicio {
    public List<Empleado> obtenerEmpleados() throws SQLException;
    public List<Asignacion> obtenerAsignacion(Empleado empleado) throws SQLException;
}
