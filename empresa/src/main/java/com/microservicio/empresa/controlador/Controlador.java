package com.microservicio.empresa.controlador;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.microservicio.empresa.dto.Empleado;
import com.microservicio.empresa.dto.rest.RespuestaAsignacion;
import com.microservicio.empresa.dto.rest.RespuestaEmpleado;
import com.microservicio.empresa.servicio.Servicio;
@RestController
public class Controlador {
	@Autowired
	private Servicio servicio;
	@RequestMapping(value="/obtener-empleado",method=RequestMethod.POST,produces="application/json;charset=utf-8")
  public @ResponseBody RespuestaEmpleado obtenerEmpleados() throws SQLException {
	  RespuestaEmpleado respuestaEmpleado=new RespuestaEmpleado();
	  respuestaEmpleado.setListaEmpleado(servicio.obtenerEmpleados());
	return respuestaEmpleado;
  }
	@RequestMapping(value="/obtener-asignacion",method=RequestMethod.POST,produces="application/json;charset=utf-8")
	  public @ResponseBody RespuestaAsignacion obtenerAsignaciones(Empleado empleado) throws SQLException {
		  RespuestaAsignacion respuestaAsignacion=new RespuestaAsignacion();
		  respuestaAsignacion.setListaAsignacion(servicio.obtenerAsignacion(empleado));
		return respuestaAsignacion;
	  }
}
