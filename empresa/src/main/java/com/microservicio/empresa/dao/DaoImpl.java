package com.microservicio.empresa.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import com.microservicio.empresa.dto.*;
@Repository
public class DaoImpl implements Dao{
@Autowired
private JdbcTemplate jdbcTemplate;
private Connection conexion;
private void obtenerConexion() throws SQLException {
	conexion=jdbcTemplate.getDataSource().getConnection();
}
private void cerrarConexion() throws SQLException {
	conexion.close();
	conexion=null;
}
	public List<Empleado> obtenerEmpleados() throws SQLException {
		List<Empleado> empleados=new ArrayList<>();
		obtenerConexion();
		Statement sentencia=conexion.createStatement();
		String sql=" SELECT e.nombres,e.apellidos,d.nombre nombre_departamento "
				+ " FROM empleado e "
				+ " JOIN departamento d ON (e.codigo_departamento=d.codigo_departamento) ";
		ResultSet resultado=sentencia.executeQuery(sql);
		while(resultado.next()) {
			Empleado empleado=new Empleado();
			empleado.setNombres(resultado.getString("nombres"));
			empleado.setApellidos(resultado.getString("apellidos"));
			empleado.setDepartamento(new Departamento());
			empleado.getDepartamento().setNombre(resultado.getString("nombre_departamento"));
			empleados.add(empleado);
		}
		resultado.close();
		sentencia.close();
		cerrarConexion();
		return empleados;
	}

	public Actividad agregarActividad() throws SQLException {
		obtenerConexion();
		
		return null;
	}

	public List<Asignacion> obtenerAsignaciones(Empleado empleado) throws SQLException {
			List<Asignacion> asignaciones=new ArrayList<>();
			obtenerConexion();
			Statement sentencia=conexion.createStatement();
			String sql ="SELECT *\r\n"
					+ "FROM asignacion\r\n"
					+ "WHERE codigo_empleado=\"A111";
			ResultSet resultado = sentencia.executeQuery(sql);
			while(resultado.next()) {
				Asignacion asignacion=new Asignacion();
			    asignacion.setId_actividad(resultado.getInt("id_actividad"));
			    asignacion.setId_asignacion(resultado.getInt("id_asignacion"));
			    asignacion.setNombre(resultado.getString("nombre"));
			    asignacion.setPresupuesto(resultado.getDouble("presupuesto"));
			    asignaciones.add(asignacion);
			}	
			resultado.close();
			sentencia.close();
			cerrarConexion();
			return asignaciones;
		}
	}


