package com.microservicio.empresa.dao;

import java.sql.SQLException;
import java.util.List;

import com.microservicio.empresa.dto.Actividad;
import com.microservicio.empresa.dto.Asignacion;
import com.microservicio.empresa.dto.Empleado;

public interface Dao {
	public List<Empleado> obtenerEmpleados() throws SQLException;
	public Actividad agregarActividad() throws SQLException;
	public List<Asignacion> obtenerAsignaciones(Empleado empleado) throws SQLException;
}
