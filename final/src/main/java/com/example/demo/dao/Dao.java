package com.example.demo.dao;

import java.sql.SQLException;
import java.util.List;

import com.example.demo.dto.Usuario;

public interface Dao {
	public List<Usuario> obtenerUsuarios() throws SQLException;
	public void agregarUsuario(Usuario usuario) throws SQLException;
}
