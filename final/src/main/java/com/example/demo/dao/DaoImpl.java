package com.example.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.Usuario;
@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Connection conexion;

    private void obtenerConexion(){
        try {
            this.conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(ResultSet resultado, Statement sentencia){
        try {
            if(resultado != null) resultado.close();
            if(sentencia != null) sentencia.close();
            this.conexion.commit();
            this.conexion.close();
            this.conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
	public List<Usuario> obtenerUsuarios() throws SQLException {
		List<Usuario> usuarios=new ArrayList<>();
		String sql="\r\n"
				+ "SELECT id_usuario,nombres,apellidos,correo,administrador,clave\r\n"
				+ "FROM usuario \r\n";
		obtenerConexion();
		PreparedStatement sentencia=conexion.prepareStatement(sql);
		ResultSet resultado=sentencia.executeQuery(sql);
		while(resultado.next()) {
			Usuario u=new Usuario();
			u.setId_usuario(resultado.getInt("id_usuario"));
			u.setNombres(resultado.getString("nombres"));
			u.setApellidos(resultado.getString("apellidos"));
			u.setClave(resultado.getString("clave"));
			u.setAdministrador(resultado.getString("administrador"));
			u.setCorreo(resultado.getString("correo"));
			usuarios.add(u);
		}
		cerrarConexion(resultado,sentencia);
		return usuarios;
	}
	public void agregarUsuario(Usuario usuario) throws SQLException {
		String sql="INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)\r\n"
				+ "VALUES (?,?,?,?,?,?)";
		obtenerConexion();
		PreparedStatement sentencia=conexion.prepareStatement(sql);
		sentencia.setInt(1, usuario.getId_usuario());
		sentencia.setString(2, usuario.getNombres());
		sentencia.setString(3,usuario.getApellidos());
		sentencia.setString(4,usuario.getCorreo());
		sentencia.setString(5, usuario.getAdministrador());
		sentencia.setString(6,usuario.getClave());
		sentencia.executeUpdate();
		cerrarConexion(null,sentencia);
	}
}
