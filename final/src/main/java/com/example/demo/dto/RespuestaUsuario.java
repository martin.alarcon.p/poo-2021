package com.example.demo.dto;

import java.util.List;

public class RespuestaUsuario {
	private List<Usuario> lista;

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}
	
}
