package com.example.demo.controlador;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.RespuestaUsuario;
import com.example.demo.dto.Usuario;
import com.example.demo.servicio.Servicio;

@RestController
@CrossOrigin(origins = {"*"})
public class Controlador {
	@Autowired
    private Servicio servicio;
	 @RequestMapping(
		        value = "obtener-usuarios",method = RequestMethod.POST,
		        produces = "application/json;charset=utf-8"
		    )
	 @ResponseBody RespuestaUsuario obtenerUsuarios() throws SQLException {
		 RespuestaUsuario respuestaUsuario=new RespuestaUsuario();
		respuestaUsuario.setLista(servicio.obtenerUsuarios());
		return respuestaUsuario;
		 
	 }
}
