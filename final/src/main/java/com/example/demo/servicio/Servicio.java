package com.example.demo.servicio;

import java.sql.SQLException;
import java.util.List;

import com.example.demo.dto.Usuario;

public interface Servicio {
	public List<Usuario> obtenerUsuarios() throws SQLException;
}
