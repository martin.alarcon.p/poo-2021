package com.example.demo.servicio;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.Dao;
import com.example.demo.dto.Usuario;
@Service
@Transactional
public class ServicioImpl implements Servicio{
	@Autowired
    private Dao dao;
	public List<Usuario> obtenerUsuarios() throws SQLException {
		return dao.obtenerUsuarios();
	}

}
