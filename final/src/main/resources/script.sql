CREATE DATABASE final;

USE final;

CREATE TABLE usuario(
	id_usuario NUMERIC(9),
	nombres VARCHAR(100),
	apellidos VARCHAR(100),
	correo VARCHAR(200),
	administrador CHAR(1),
	clave VARCHAR(200)
);


INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)
VALUES (1,"Rony","Hancco","ronyh@gmail.com",1,"mariadb");

INSERT INTO usuario(id_usuario,nombres,apellidos,correo,administrador,clave)
VALUES (2,"Martin","Alarcon","martin.alarcon.p@uni.pe",0,"20200252H");

SELECT id_usuario,nombres,apellidos,correo,administrador,clave
FROM usuario 
WHERE administrador="0";