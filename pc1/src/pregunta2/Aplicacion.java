package pregunta2;

import java.util.*;

public class Aplicacion {
	public static void main(String args[]){
      Area area=new Area();
      double lado=getLado();
      int numeroDeLados=getNumeroDeLados();
      System.out.println(area.getArea(lado,numeroDeLados));
      Perimetro perimetro=new Perimetro();
      System.out.println(perimetro.getPerimetro(lado,numeroDeLados));
      Diagonales diagonales=new Diagonales();
      System.out.println(diagonales.getDiagonales(numeroDeLados));
      }
	public static double getLado() {
		Scanner entrada=new Scanner(System.in);
		System.out.print("Digite la longitud del lado: ");
		double lado=entrada.nextDouble();
		return lado;
	}
	public static int getNumeroDeLados() {
		Scanner entrada=new Scanner(System.in);
		System.out.print("Digite el numero de lados: ");
		int numeroDeLados=entrada.nextInt();
		return numeroDeLados;
	}
}
