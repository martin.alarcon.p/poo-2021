package pregunta2;

public class Area {
     public static double getArea(double lado, int numeroDeLados) {
    	Apotema apotema=new Apotema();
    	double apotema2=apotema.getApotema(lado,numeroDeLados);
    	Perimetro perimetro=new Perimetro();
    	double perimetro2=perimetro.getPerimetro(lado,numeroDeLados);
    	double area=apotema2*perimetro2/2;
    	return area;
     }
}
