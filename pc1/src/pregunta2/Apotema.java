package pregunta2;

public  class Apotema {
      public double getApotema(double lado, int numeroDeLados) {
    	  double angulo=360/numeroDeLados;
    	  double apotema=lado/(2*Math.tan(angulo));
    	  return apotema;
      }
}
