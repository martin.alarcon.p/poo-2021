package lab2;

import java.util.Scanner;

class cifradoCesar{
	
	public static void main(String[] args) {
		System.out.println(codificar(getFrase(),getDesplazamiento()));
		System.out.println(decodificar(getFrase(),getDesplazamiento()));
	}
	
	
	
	 static String codificar(String frase,int desplazamiento) {
		String frase2="";
		String cadena="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		for(int i=0;i<frase.length();i++){
			for(int j=0;j<cadena.length();j++) {
				if (frase.charAt(i)==cadena.charAt(j)){
					if (desplazamiento+j>=cadena.length()) {
						frase2=frase2+cadena.charAt((desplazamiento+j)%cadena.length());
					}
					else {
						frase2=frase2+cadena.charAt(desplazamiento+j);
					}
				}
			}
		}
		return frase2;
	}
	 static String decodificar(String frase,int desplazamiento) {
			String frase2="";
			String cadena="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			for(int i=0;i<frase.length();i++){
				for(int j=0;j<cadena.length();j++) {
					if (frase.charAt(i)==cadena.charAt(j)){
						if (j-desplazamiento<0) {
							frase2=frase2+cadena.charAt((cadena.length()+j-desplazamiento%cadena.length()));
						}
						else {
							frase2=frase2+cadena.charAt(j-desplazamiento);
						}
					}
				}
			}
			return frase2;
		}
	 static String getFrase(){
		 Scanner entrada=new Scanner(System.in);
		 System.out.print("Ingrese la frase: ");
		 String frase=entrada.nextLine();
		 return frase;
	 }
	 static int getDesplazamiento() {
		 Scanner entrada=new Scanner(System.in);
		 System.out.print("Ingrese el desplazamiento: ");
		 int desplazamiento=entrada.nextInt();
		 return desplazamiento;
	 }
}