package fibonacci;

public class Fibonacci {
       public int[] calcularFibonacci(int cantidadTerminos) {
    	   int[] arreglo =new int[cantidadTerminos];
    	   int sumaAnterior2=0;
           int sumaAnterior1=1;
           int suma;
    	   for (int i=0;i<cantidadTerminos;i++) {
    		   if (i==0 || i==1) {
    			   arreglo[i]=i;
    		   }else {
    		   suma=sumaAnterior1+sumaAnterior2;
    		   sumaAnterior2=sumaAnterior1;
    		   sumaAnterior1=suma;
    		   arreglo[i]=suma;}
    	   }
    	   return arreglo;
       }
}
