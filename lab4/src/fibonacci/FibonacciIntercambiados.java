package fibonacci;

public class FibonacciIntercambiados {
	public int[] calcularFibonacciIntercambiado(int cantidadTerminos) {
 	   int[] arreglo =new int[cantidadTerminos];
 	   int sumaAnterior2=0;
        int sumaAnterior1=1;
        int suma;
        int negativo;
 	   for (int i=0;i<cantidadTerminos;i++) {
 		   if (i==0 || i==1) {
 			   suma=i;
 		   }else {
 		   suma=sumaAnterior1+sumaAnterior2;
 		   sumaAnterior2=sumaAnterior1;
 		   sumaAnterior1=suma; }
 		  if (i%2==1){
			   negativo=suma*-1;
			   arreglo[i]=negativo;
		   }
		   else {
		   arreglo[i]=suma;}
		   }
 	   return arreglo;
    }
}
