package libros;

public interface Servicio {
    public void buscarLibroPorTitulo(String titulo);
    public void buscarLibroPorAutores(String autores);
    public void buscarLibroPorCodigo(String codigo);
    public void datosLibro(Libro libro);
}
