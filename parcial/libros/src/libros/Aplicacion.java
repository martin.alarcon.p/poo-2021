package libros;

import java.util.List;
import java.util.ArrayList;

public class Aplicacion {
      public static void main(String[] args){
    	  Libro libro1=new Libro();
    	  libro1.setAutores("Cesar Vallejo");
    	  libro1.setAņoPublicacion(1897);
    	  libro1.setCapitulo("Capitulo1: Ella\nCapitulo 2: No me ama");
    	  libro1.setCodigo("BF101U");
    	  libro1.setDescripcion("Es una historia de amor");
    	  libro1.setTitulo("Trilce");
    	  Libro libro2=new Libro();
    	  libro2.setAutores("Garcilaso");
    	  libro2.setAņoPublicacion(1643);
    	  libro2.setCapitulo("Capitulo1: Es Importante\nCapitulo2: Mi familia");
    	  libro2.setCodigo("FB303W");
    	  libro2.setDescripcion("Habla acerca de su familia");
    	  libro2.setTitulo("Comentarios Reales");
    	  List<Libro> libros=new ArrayList<>();
    	  libros.add(libro1);
    	  libros.add(libro2);
    	  ServicioImpl servicio=new ServicioImpl();
    	  servicio.setLibros(libros);
    	  servicio.buscarLibroPorAutores("Garcilaso");
    	  servicio.buscarLibroPorCodigo("BF101U");
    	  servicio.buscarLibroPorTitulo("Trilce");
      }
}
