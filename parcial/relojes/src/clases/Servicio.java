package clases;

import java.util.List;
import java.util.ArrayList;

public class Servicio {
     List<Reloj> relojes=new ArrayList<>();
     public List<Reloj> getLista() {
		return relojes;
	}
	public void registrarVentas(Reloj reloj,int cantidad,Double precioTotal,String producto) {
		reloj.setCantidad(cantidad);
		reloj.setPrecioTotal(precioTotal);
		reloj.setProducto(producto);
		this.relojes=relojes;
		relojes.add(reloj);
     }
     public void generarReporte(List<Reloj>relojes) {
    	 this.relojes=relojes;
    	 for (Reloj reloj:relojes) {
    		 System.out.println(reloj.getProducto());
    		 System.out.println(reloj.getCantidad());
    		 System.out.println(reloj.getPrecioTotal());
    	 }
     
     
}
	public List<Reloj> getRelojes() {
		return relojes;
	}
	public void setRelojes(List<Reloj> relojes) {
		this.relojes = relojes;
	}
     
     
}
