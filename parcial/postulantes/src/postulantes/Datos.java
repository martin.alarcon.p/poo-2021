package postulantes;

public class Datos {
     private String numeroDocumento;
     private String nombres;
     private String apellidos;
     private NivelEducativo nivelEducativo;
     private String direccion;
     private String numeroTelefonico;
     private int edad;
     private int notaDeEvaluacion;
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public NivelEducativo getNivelEducativo() {
		return nivelEducativo;
	}
	public void setNivelEducativo(NivelEducativo nivelEducativo) {
		this.nivelEducativo = nivelEducativo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}
	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public int getNotaDeEvaluacion() {
		return notaDeEvaluacion;
	}
	public void setNotaDeEvaluacion(int notaDeEvaluacion) {
		this.notaDeEvaluacion = notaDeEvaluacion;
	}
	
     
}
