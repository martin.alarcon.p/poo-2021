package postulantes;

import java.util.List;
import java.util.ArrayList;

public class ServicioImpl implements Servicio {
    List<Datos> listaPostulantes=new ArrayList<>();
    public void Registrar(Datos dato,String numeroDocumento, String nombres, String apellidos, NivelEducativo nivelEducativo,
			String direccion, String numeroTelefonico, int edad, int notaDeEvaluacion) {
		dato.setApellidos(apellidos);
		dato.setNombres(nombres);
		dato.setDireccion(direccion);
		dato.setEdad(edad);
		dato.setNotaDeEvaluacion(notaDeEvaluacion);
		dato.setNumeroDocumento(numeroDocumento);
		dato.setNumeroTelefonico(numeroTelefonico);
		dato.setNivelEducativo(nivelEducativo);
		listaPostulantes.add(dato);
	}
     public void modificarNota(String numeroDocumento,int nota) {
    	 this.listaPostulantes=listaPostulantes;
    	 for (Datos alumno:listaPostulantes) {
    		 if (alumno.getNumeroDocumento().equals(numeroDocumento)){
    			 alumno.setNotaDeEvaluacion(nota);
    		 }
    	 }
     }
}
