import { Component, OnInit } from '@angular/core';
import { ApiService } from '../ApiService';
import { Usuario } from '../interfaces';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
  styleUrls: ['./busqueda.component.scss']
})
export class BusquedaComponent implements OnInit {

  constructor(private api:ApiService) { }
  nombre: string="";
  listaUsuario:Usuario[]=[];  
  ngOnInit(): void {
  }
  buscar():void{
    console.log(this.nombre);
    this.api.obtenerProductos().subscribe( respuesta =>{
      this.listaUsuario = respuesta.listaUsuario;
    });

} 
}