export interface Usuario{
	 id_usuario?: number,
	 nombres?:string,
	 apellidos?: string,
	correo?:string,
	administrador?:string,
	clave?:string
}
export interface RespuestaUsuario{
	listaUsuario: Usuario[]
}
export interface SesionUsuario{
    id: number;
    listaUsuario?: Usuario[];
}